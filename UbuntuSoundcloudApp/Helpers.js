function getUserTracksUrl(userId) {
    return getUserUri(userId) + "/tracks" + "?client_id=" + getClientId() + getPaginationString()
}

function getFavoritesUri(userId) {
    return getUserUri(userId) + "/favorites" + "?client_id=" + getClientId() + getPaginationString()
}

function getPaginationString() {
    return "&linked_partitioning=1"
}

function getClientIdString() {
    return "?client_id=" + getClientId()
}

function getUserUri(userId) {
    return "https://api.soundcloud.com/users/" + userId
}

function getClientId() {
    return "b15be07e0afa1dea748c1cdd63f9e56e"
}

function getApiHost() {
    return "http://api.soundcloud.com"
}

function getFormattedTime(durationInMillis) {
    // Convert input to int
    var input = durationInMillis * 1

    var date = new Date(input)
    var hours = date.getUTCHours()
    var minutes = date.getUTCMinutes()
    var seconds = date.getUTCSeconds()

    if (hours == 0) {
        if (minutes == 0) {
            if (seconds == 0) {
                return "00:00"
            } else if (seconds < 10) {
                return "00:0" + seconds
            } else if (seconds < 60) {
                return "00:" + seconds
            }
        } else if (minutes < 10) {
            if (seconds == 0) {
                return "0" + minutes + ":00"
            } else if (seconds < 10) {
                return "0" + minutes + ":0" + seconds
            } else if (seconds < 60) {
                return "0" + minutes + ":" + seconds
            }
        } else if (minutes < 60) {
            if (seconds == 0) {
                return minutes + ":00"
            } else if (seconds < 10) {
                return minutes + ":0" + seconds
            } else if (seconds < 60) {
                return minutes + ":" + seconds
            }
        }
    } else if (hours < 10) {
        var hourString = "0" + hours
        if (minutes == 0) {
            if (seconds == 0) {
                return hourString + ":" + "00:00"
            } else if (seconds < 10) {
                return hourString + ":" + "00:0" + seconds
            } else if (seconds < 60) {
                return hourString + ":" + "00:" + seconds
            }
        } else if (minutes < 10) {
            if (seconds == 0) {
                return hourString + ":" + "0" + minutes + ":00"
            } else if (seconds < 10) {
                return hourString + ":" + "0" + minutes + ":0" + seconds
            } else if (seconds < 60) {
                return hourString + ":" + "0" + minutes + ":" + seconds
            }
        } else if (minutes < 60) {
            if (seconds == 0) {
                return hourString + ":" + minutes + ":00"
            } else if (seconds < 10) {
                return hourString + ":" + minutes + ":0" + seconds
            } else if (seconds < 60) {
                return hourString + ":" + minutes + ":" + seconds
            }
        }
    } else {
        // We are not expecting any audio that is more than 10 hours long
        return "Invalid time"
    }
}
