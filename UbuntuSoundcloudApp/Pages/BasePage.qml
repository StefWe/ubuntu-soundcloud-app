import QtQuick 2.4
import QtMultimedia 5.5
import QtQuick.LocalStorage 2.0

import Ubuntu.Components 1.3

import UbuntuSoundcloudApp 1.0

import "../Components"

import "../Helpers.js" as Helpers
import "../DbHelpers.js" as Db


Page {

    id: basePage

    property alias title: pageHeader.title
    property alias pageHeader: pageHeader
    property alias httpConnection: httpConnection
    property alias mediaPlayer: mediaPlayer
    property alias playbackControls: playbackControls
    property alias pageLoadingProgress: pageLoadingProgress

    property string iconPlay: "media-playback-start"
    property string iconPause: "media-playback-pause"
    property string pageTitleTracks: i18n.tr("My Tracks")
    property string pageTitleFavorites: i18n.tr("Favorites")
    property string userId: undefined

    function runLoadingIndicator() {
        contentLoader.sourceComponent = undefined
        pageLoadingProgress.parent = contentLoader
        pageLoadingProgress.running = true
    }

    function playAndUpdateUi() {
        mediaPlayer.play()
        playbackIcon = iconPause
    }

    function pauseAndUpdateUi() {
        mediaPlayer.pause()
        playbackIcon = iconPlay
    }

    header: PageHeader {
        id: pageHeader

        StyleHints {
            foregroundColor: UbuntuColors.porcelain
            backgroundColor: UbuntuColors.coolGrey
            dividerColor: UbuntuColors.orange
        }

        leadingActionBar.actions: [
            Action {
                text: pageTitleTracks
                onTriggered: {
                    basePage.runLoadingIndicator()
                    pageHeader.title = text
                    Db.setLastPage(LocalStorage, text)
                    if (Db.shouldLoadMyTracksFromDb(LocalStorage) === "true") {
                        loadContentFromDb('tracks')
                    } else {
                        httpConnection.requestTracks(Helpers.getUserTracksUrl(userId))
                    }
                }
            },
            Action {
                text: pageTitleFavorites
                onTriggered: {
                    basePage.runLoadingIndicator()
                    pageHeader.title = text
                    Db.setLastPage(LocalStorage, text)
                    if (Db.shouldLoadFavoritesFromDb(LocalStorage) === "true") {
                        loadContentFromDb('fav')
                    } else {
                        httpConnection.requestTracks(Helpers.getFavoritesUri(userId))
                    }
                }
            }
        ]

        trailingActionBar.actions: [
            Action {
                iconName: "reload"
                text: "Refresh"
                onTriggered: {
                    basePage.runLoadingIndicator()
                    if (pageHeader.title === pageTitleTracks) {
                        Db.clearMyTracks(LocalStorage)
                        httpConnection.requestTracks(Helpers.getUserTracksUrl(userId))
                    } else if (pageHeader.title === pageTitleFavorites) {
                        Db.clearFavorites(LocalStorage)
                        httpConnection.requestTracks(Helpers.getFavoritesUri(userId))
                    }
                }
            }
        ]
    }

    PlayerControls {
        id: playbackControls
        width: parent.width
        height: units.gu(6.5)
        anchors.bottom: parent.bottom
        z: 10

        onPlaybackButtonClicked: {
            if (mediaPlayer.playbackState == 1) {
                basePage.pauseAndUpdateUi()
            } else {
                if (mediaPlayer.source != "") {
                    basePage.playAndUpdateUi()
                }
            }
        }
    }

    HttpUrlConnection {
        id: httpConnection
    }

    MediaPlayer {
        id: mediaPlayer
    }

    ActivityIndicator {
        id: pageLoadingProgress
        anchors.centerIn: parent
    }

    function loadContentFromDb(type) {
        var data = ""
        var rawData = ""
        if (type === "fav") {
            data = Db.getFavorites(LocalStorage)
            if (data == "") {
                pageLoadingProgress.running = false
                return
            }
            rawData = JSON.parse(data)
        } else if (type === "tracks") {
            data = Db.getMyTracks(LocalStorage)
            if (data == "") {
                pageLoadingProgress.running = false
                return
            }
            rawData = JSON.parse(data)
        }

        listModel.clear()
        for (var i = 0; i < rawData.length; i++) {
            var trackTitle = rawData[i].title
            var trackDuration = rawData[i].duration
            var trackUrl = rawData[i].url
            var trackArt = rawData[i].art
            var details = {
                title: trackTitle,
                duration: trackDuration,
                url: trackUrl,
                art: trackArt
            }

            listModel.append(details)
        }
        contentLoader.sourceComponent = loadee
        pageLoadingProgress.running = false
    }
}

