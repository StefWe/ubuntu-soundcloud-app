import QtQuick 2.4
import QtQuick.LocalStorage 2.0
import QtMultimedia 5.5

import Ubuntu.Components 1.3

import UbuntuSoundcloudApp 1.0

import "../Components"

import "../Helpers.js" as Helpers
import "../DbHelpers.js" as Db


BasePage {
    id: mainPage
    title: i18n.tr(listToLoad)

    property string listToLoad: undefined
    property bool resume: false

    Component.onCompleted: {
        pageLoadingProgress.running = true
        if (listToLoad == pageTitleFavorites) {
            if (Db.shouldLoadFavoritesFromDb(LocalStorage) === "true") {
                loadContentFromDb('fav')
            } else {
                httpConnection.requestTracks(Helpers.getFavoritesUri(userId))
            }
        } else if (listToLoad == pageTitleTracks) {
            if (Db.shouldLoadMyTracksFromDb(LocalStorage) === "true") {
                loadContentFromDb('tracks')
            } else {
                httpConnection.requestTracks(Helpers.getUserTracksUrl(userId))
            }
        }
    }

    ListModel {
        id: listModel
    }

    Loader {
        id: pageLoader
        width: parent.width
        height: parent.height
        asynchronous: true
    }

    Loader {
        id: contentLoader
        width: parent.width
        height: parent.height
        anchors.top: header.bottom
        anchors.bottom: playbackControls.top
        asynchronous: true
        anchors.bottomMargin: units.gu(.5)
    }

    Component {
        id: loadee

        Flickable {
            id: mainFlickable
            flickableDirection: Flickable.VerticalFlick
            contentHeight: item.height * listModel.count
            clip: true

            Column {
                id: item
                width: parent.width
                height: units.gu(10)
                Repeater {
                    model: listModel

                    TrackDelegate {
                        id: delegate
                        width: parent.width
                        height: parent.height
                        art: listModel.get(index).art
                        title: listModel.get(index).title
                        duration: Helpers.getFormattedTime(listModel.get(index).duration)

                        onItemClicked: {
                            resume = false
                            var trackUrl = listModel.get(index).url
                            var fileName = httpConnection.getTrackIdFromUrl(trackUrl)
                            var urlToPlay = bufferer.getCachedFilePath(fileName)
                            if (mediaPlayer.playbackState == 1) {
                                if (mediaPlayer.source == "file://" + urlToPlay) {
                                    mediaPlayer.pause()
                                    return
                                }
                                mediaPlayer.stop()
                            }

                            var doesCacheExist = bufferer.doesCacheFileExist(fileName)
                            if (doesCacheExist === "true") {
                                mediaPlayer.source = urlToPlay
                                updateUiForPlayback()
                                mediaPlayer.play()

                                if (bufferer.getFileSizeOnStorage(fileName) < Db.getFileSize(
                                            LocalStorage, fileName)) {

                                    // Resume Download
                                    resume = true
                                    httpConnection.getStreamUrl(trackUrl + Helpers.getClientIdString())
                                    console.log(bufferer.getFileSizeOnStorage(fileName))
                                    console.log(Db.getFileSize(LocalStorage, fileName))
                                }
                            } else {
                                httpConnection.getStreamUrl(trackUrl + Helpers.getClientIdString())
                                updateUiForPlayback()
                            }

                            function updateUiForPlayback() {
                                hightlight.parent = delegate
                                hightlight.visible = true
                                pageLoadingProgress.parent = trackArt
                                pageLoadingProgress.running = true
                                playbackControls.trackTitle = listModel.get(index).title
                                playbackControls.maxValue = listModel.get(index).duration
                            }

                        }
                    }
                }
            }
        }
    }

    Rectangle {
        id: hightlight
        anchors.fill: parent
        opacity: 0.1
        color: UbuntuColors.graphite
        visible: false
    }

    CustomBufferer {
        id: bufferer

        onDownloadProgress: {
            playbackControls.totalBuffer = total
            playbackControls.bufferProgress = downloaded
        }

        onBroadcastNewUrl: {
            mediaPlayer.source = url
            mediaPlayer.play()
        }

        onFileSizeDetermined: {
            console.log(name + " : " + size)
            Db.saveFileSize(LocalStorage, name, size)
        }
    }

    Connections {
        target: httpConnection

        onStreamUrlResolved: {
            var jsonData = JSON.parse(data)
            if (resume === true) {
                bufferer.resumeDownload(jsonData.location, trackId, Db.getFileSize(LocalStorage, trackId))
            } else {
                bufferer.downloadTrack(jsonData.location, trackId)
                playbackControls.playbackIcon = iconPause
            }
        }

        onFavoritesLoaded: {
            populateFavoritesList(data, type);
        }

        function populateFavoritesList(data, type) {
            listModel.clear()
            var result = JSON.parse(data)
            for (var i = 0; i < result.collection.length; i++) {
                var item = result.collection[i]

                var trackTitle = item.title
                var trackDuration = item.duration
                var trackUrl = item.stream_url
                var trackArt = item.artwork_url === null ? "../UbuntuSoundcloudApp.png": item.artwork_url

                var details = {title: trackTitle, duration: trackDuration, url: trackUrl, art: trackArt}
                listModel.append(details)
                if (type === "favorites") {
                    Db.addFavoriteItem(LocalStorage, trackTitle, trackDuration, trackUrl, trackArt)
                } else if (type === "tracks") {
                    Db.addMyTrackItem(LocalStorage, trackTitle, trackDuration, trackUrl, trackArt)
                }
            }

            if (type === "favorites") {
                Db.setLoadFavoritesFromDb(LocalStorage, "true")
            } else if (type === "tracks") {
                Db.setLoadMyTracksFromDb(LocalStorage, "true")
            }

            pageLoadingProgress.running = false
            contentLoader.sourceComponent = loadee
        }
    }

    Connections {
        target: mediaPlayer

        onPlaybackStateChanged: {
            if (mediaPlayer.playbackState == 1) {
                pageLoadingProgress.running = false
                playbackControls.playbackIcon = iconPause
            }
        }

        onPositionChanged: {
            playbackControls.progress = mediaPlayer.position
        }
    }
}
