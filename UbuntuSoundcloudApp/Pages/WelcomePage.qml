import QtQuick 2.4

import Ubuntu.Components 1.3

Rectangle {
    color: UbuntuColors.coolGrey

    property alias letsGoButton: buttonSubmit
    property alias usernameEntry: usernameEntry

    signal submit(string username)

    Rectangle {
        color: UbuntuColors.coolGrey
        width: parent.width
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: units.gu(2)

        TextField {
            id: usernameEntry
            width: parent.width
            height: units.gu(5)
            anchors.top: parent.top
            anchors.topMargin: units.gu(2)
            font.pixelSize: height / 1.4
            hasClearButton: false
            inputMethodHints: Qt.ImhNoPredictiveText
            Component.onCompleted: {
                forceActiveFocus()
            }
        }

        Button {
            id: buttonSubmit
            anchors.top: usernameEntry.bottom
            anchors.topMargin: units.gu(1)
            anchors.right: parent.right
            text: i18n.tr("Lets go!")
            color: UbuntuColors.orange
            height: units.gu(5)
            width: height * 3

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    submit(usernameEntry.text)
                }
            }
        }

        Text {
            id: message
            width: parent.width
            anchors.top: buttonSubmit.bottom
            anchors.topMargin: units.gu(4)
            text: "Enter your <font color='#e95420'><b>Soundcloud</b></font> " +
                  "username and press 'Lets go!' button above."
            elide: Text.ElideRight
            color: UbuntuColors.porcelain
            wrapMode: Text.Wrap
            clip: true
            maximumLineCount: 5
            font.pixelSize: units.gu(3.5)
        }
    }
}

