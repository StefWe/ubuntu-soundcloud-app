function isRunningForTheFirstTime(LocalStorage) {
    var db = ensureDbCreated(LocalStorage)
    var rawvalue = getStringValueByKey(db, "first_run")
    if (rawvalue === "") {
        return "true"
    }
    return rawvalue
}

function setIsRunningForTheFirstTime(LocalStorage, value) {
    var db = ensureDbCreated(LocalStorage)
    saveStringKeyValue(db, "first_run", value)
}

function saveUserId(LocalStorage, value) {
    var db = ensureDbCreated(LocalStorage)
    saveStringKeyValue(db, "user_id", value)
}

function getUserId(LocalStorage) {
    var db = ensureDbCreated(LocalStorage)
    return getStringValueByKey(db, "user_id")
}

function setLastPage(LocalStorage, value) {
    var db = ensureDbCreated(LocalStorage)
    saveStringKeyValue(db, "last_page", value)
}

function getLastPage(LocalStorage) {
    var db = ensureDbCreated(LocalStorage)
    return getStringValueByKey(db, "last_page")
}
function setLoadFavoritesFromDb(LocalStorage, value) {
    var db = ensureDbCreated(LocalStorage)
    saveStringKeyValue(db, "load_favorites_from_db", value)
}

function setLoadMyTracksFromDb(LocalStorage, value) {
    var db = ensureDbCreated(LocalStorage)
    saveStringKeyValue(db, "load_my_tracks_from_db", value)
}

function saveFileSize(LocalStorage, name, size) {
    var db = ensureDbCreated(LocalStorage)
    saveStringKeyValue(db, name, size)
}

function getFileSize(LocalStorage, name) {
    var db = ensureDbCreated(LocalStorage)
    return getStringValueByKey(db, name)
}

function shouldLoadFavoritesFromDb(LocalStorage) {
    var db = ensureDbCreated(LocalStorage)
    var rawvalue = getStringValueByKey(db, "load_favorites_from_db")
    if (rawvalue === "") {
        return "false"
    }
    return rawvalue
}

function shouldLoadMyTracksFromDb(LocalStorage) {
    var db = ensureDbCreated(LocalStorage)
    var rawvalue = getStringValueByKey(db, "load_my_tracks_from_db")
    if (rawvalue === "") {
        return "false"
    }
    return rawvalue
}

function saveStringKeyValue(db, key, value) {
    db.transaction(
        function(tx) {
            ensurePreferencesTableExists(tx)
            tx.executeSql(
                'INSERT OR REPLACE INTO SharedPreferences VALUES(?, ?)',
                [ key.toString(), value.toString() ]
            );
        }
    )
}

function getStringValueByKey(db, key) {
    var value = ""
    var query = 'SELECT * FROM SharedPreferences WHERE key="' + key.toString() + '"'
    db.transaction(
        function(tx) {
            ensurePreferencesTableExists(tx)
            var rs = tx.executeSql(query);
            if (rs.rows.length !== 0) {
                value = rs.rows.item(0).value
            }
        }
    )
    return value
}

function addFavoriteItem(LocalStorage, title, duration, url, art) {
    var db = ensureDbCreated(LocalStorage)
    _insert_favorite(db, title, duration, url, art)
}

function _insert_favorite(db, title, duration, url, art) {
    db.transaction(
        function(tx) {
            ensureFavoritesTableExists(tx)
            tx.executeSql(
                'INSERT OR REPLACE INTO Favorites VALUES(?, ?, ?, ?)',
                [ title.toString(), duration.toString(), url.toString(), art.toString() ]
            );
        }
    )
}

function addMyTrackItem(LocalStorage, title, duration, url, art) {
    var db = ensureDbCreated(LocalStorage)
    _insert_track(db, title, duration, url, art)
}

function _insert_track(db, title, duration, url, art) {
    db.transaction(
        function(tx) {
            ensureMyTracksTableExists(tx)
            tx.executeSql(
                'INSERT OR REPLACE INTO MyTracks VALUES(?, ?, ?, ?)',
                [ title.toString(), duration.toString(), url.toString(), art.toString() ]
            );
        }
    )
}

function getFavorites(LocalStorage) {
    var db = ensureDbCreated(LocalStorage)
    var query = 'SELECT * FROM Favorites'
    return _getDataFromDb(db, "fav", query)
}

function getMyTracks(LocalStorage) {
    var db = ensureDbCreated(LocalStorage)
    var query = 'SELECT * FROM MyTracks'
    return _getDataFromDb(db, "myTracks", query)
}

function _getDataFromDb(db, dataType, query) {
    var data = ""
    db.transaction(
        function(tx) {
            if (dataType === "fav") {
                ensureFavoritesTableExists(tx);
            } else if (dataType === "myTracks") {
                ensureMyTracksTableExists(tx);
            }

            var rs = tx.executeSql(query);
            if (rs.rows.length > 1) {
                var d = ""
                d += '['
                for (var i = 0; i < rs.rows.length; i++) {
                    d += '{"title":' + '"' + addslashes(rs.rows.item(i).title) + '"' + ', ' +
                          '"duration":' + rs.rows.item(i).duration + ', ' +
                          '"url":' + '"' + rs.rows.item(i).url + '"' + ', ' +
                          '"art":' + '"' + rs.rows.item(i).art + '"' + '}' + ', '
                }
                var dd = d.substring(0, d.length-2)
                dd += ']'
                data = dd
            }
        }
    )
    return data
}

function addslashes( str ) {
    // JS sucks.
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}

function clearFavorites(LocalStorage) {
    var db = ensureDbCreated(LocalStorage)
    _clearTable(db, "Favorites")
}

function clearMyTracks(LocalStorage) {
    var db = ensureDbCreated(LocalStorage)
    _clearTable(db, "MyTracks")
}

function _clearTable(db, table_name) {
    db.transaction(
        function(tx) {
            if (table_name === "Favorites") {
                ensureFavoritesTableExists(tx);
            } else if (table_name === "MyTracks") {
                ensureMyTracksTableExists(tx);
            }
            tx.executeSql('DROP TABLE IF EXISTS ' + table_name);
        }
    )
}

function ensureDbCreated(LocalStorage) {
    return LocalStorage.openDatabaseSync("SoundcloudAppDb", "1.0", "Soundcloud app SQL", 1000000);
}

function ensurePreferencesTableExists(tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS SharedPreferences(key TEXT UNIQUE, value TEXT)');
}

function ensureFavoritesTableExists(tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS Favorites(title TEXT, duration TEXT, url TEXT UNIQUE, art TEXT)');
}

function ensureMyTracksTableExists(tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS MyTracks(title TEXT, duration TEXT, url TEXT UNIQUE, art TEXT)');
}
