import QtQuick 2.4
import QtQuick.LocalStorage 2.0

import Ubuntu.Components 1.3

import UbuntuSoundcloudApp 1.0

import "Pages"

import "DbHelpers.js" as Db


MainView {
    id: mainView
    objectName: "mainView"
    applicationName: "ubuntusoundcloudapp.stefwe"
    automaticOrientation: false
    anchorToKeyboard: true

    width: units.gu(80)
    height: units.gu(70)

    property string pageTitleTracks: i18n.tr("My Tracks")
    property string pageTitleFavorites: i18n.tr("Favorites")
    property string tracksListToLoad: pageTitleTracks

    Loader {
        id: pageLoader
        asynchronous: true
    }

    Component.onCompleted: {
        if (Db.isRunningForTheFirstTime(LocalStorage) === "true") {
            pageLoader.sourceComponent = componentPageWelcome
        } else {
            var lastPage = Db.getLastPage(LocalStorage)
            if (lastPage === pageTitleTracks || lastPage === "") {
                tracksListToLoad = pageTitleTracks
            } else if (lastPage === pageTitleFavorites) {
                tracksListToLoad = pageTitleFavorites
            }
            pageLoader.sourceComponent = componentTracksList
        }
    }

    Component {
        id: componentTracksList
        TracksListPage {
            listToLoad: tracksListToLoad
            userId: Db.getUserId(LocalStorage)
        }
    }

    Component {
        id: componentPageWelcome
        WelcomePage {
            id: welcomepage
            width: mainView.width
            height: mainView.height

            onSubmit: {
                if (username.length == 0) {
                    console.log("Empty")
                } else {
                    connection.resolveUser(username)
                    usernameEntry.enabled = false
                    letsGoButton.enabled = false
                }
            }

            Connections {
                target: connection

                onUserResolved: {
                    connection.requestUserInfo(JSON.parse(data).location)
                }

                onUserResolveError: {
                    if (data === "404") {
                        // User does not exist
                    } else if (data === "unknown") {
                        // Show dialog "an unknown error occured"
                    }
                    welcomepage.usernameEntry.enabled = true
                    welcomepage.letsGoButton.enabled = true
                }

                onUserInfoLoaded: {
                    // TODO: Save user ID into the database here and redirect to tracks page
                    console.log(JSON.parse(data).id)
                    Db.saveUserId(LocalStorage, JSON.parse(data).id)
                    Db.setIsRunningForTheFirstTime(LocalStorage, "false")
                    pageLoader.sourceComponent = componentTracksList
                }

                onUserInfoError: {
                    console.log(data)
                }
            }
        }
    }

    HttpUrlConnection {
        id: connection
    }
}
