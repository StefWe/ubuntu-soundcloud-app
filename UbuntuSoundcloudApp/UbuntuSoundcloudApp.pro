TEMPLATE = aux
TARGET = UbuntuSoundcloudApp

RESOURCES += UbuntuSoundcloudApp.qrc

QML_FILES += $$files(Main.qml,true) \
             $$quote(Components) \
             $$quote(Pages) \
             $$files(*.js,true)

CONF_FILES +=  UbuntuSoundcloudApp.apparmor \
               UbuntuSoundcloudApp.png

OTHER_FILES += $${CONF_FILES} \
               $${QML_FILES} \
               UbuntuSoundcloudApp.desktop 

#specify where the qml/js files are installed to
qml_files.path = /UbuntuSoundcloudApp
qml_files.files += $${QML_FILES}

#specify where the config files are installed to
config_files.path = /UbuntuSoundcloudApp
config_files.files += $${CONF_FILES}

#install the desktop file, a translated version is automatically created in 
#the build directory
desktop_file.path = /UbuntuSoundcloudApp
desktop_file.files = $$OUT_PWD/UbuntuSoundcloudApp.desktop 
desktop_file.CONFIG += no_check_exist 

INSTALLS+=config_files qml_files desktop_file

DISTFILES += \
    Helpers.js \
    DbHelpers.js \
    Components/CustomProgressBar.qml \
    Components/PlayerControls.qml \
    Components/TrackDelegate.qml \
    Pages/BasePage.qml \
    Pages/TracksListPage.qml \
    Pages/WelcomePage.qml
