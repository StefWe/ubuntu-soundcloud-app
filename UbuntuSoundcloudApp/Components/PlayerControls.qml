import QtQuick 2.4

import Ubuntu.Components 1.3

Item {

    property alias progress: playbackPosition.progress
    property alias maxValue: playbackPosition.totalProgress
    property alias bufferProgress: playbackPosition.bufferProgress
    property alias totalBuffer: playbackPosition.totalBuffer
    property alias playbackIcon: buttonPlay.iconName
    property alias trackTitle: currentTrackTitle.text

    signal playbackButtonClicked

    Rectangle {
        id: bottomContainer
        width: parent.width
        height: units.gu(6)
        color: UbuntuColors.coolGrey
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.top: positionContainer.bottom

        Button {
            id: buttonPlay
            iconName: "media-playback-start"
            color: "transparent"
            height: parent.height
            width: units.gu(9)
            anchors.verticalCenter: parent.verticalCenter

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    playbackButtonClicked();
                }
            }
        }

        Text {
            id: currentTrackTitle
            width: parent.width
            anchors.left: buttonPlay.right
            anchors.leftMargin: units.gu(1)
            anchors.right: parent.right
            anchors.rightMargin: units.gu(1)
            anchors.verticalCenter: parent.verticalCenter
            wrapMode: Text.Wrap
            elide: Text.ElideRight
            clip: true
            maximumLineCount: 2
            color: UbuntuColors.porcelain
        }
    }

    Rectangle {
        id: positionContainer
        height: units.gu(.5)
        width: parent.width
        anchors.bottom: bottomContainer.top

        CustomProgressBar {
            id: playbackPosition
            width: parent.width
            height: parent.height
            anchors.left: parent.left
            anchors.top: parent.top
        }
    }
}

