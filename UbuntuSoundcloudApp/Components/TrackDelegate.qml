import QtQuick 2.4

import Ubuntu.Components 1.3

Rectangle {

    property alias trackArt: artworkContainer
    property alias art: trackArt.source
    property alias title: trackTitle.text
    property alias duration: trackDuration.text

    signal itemClicked

    UbuntuShape {
        id: artworkContainer
        radius: "medium"
        anchors.left: parent.left
        anchors.leftMargin: units.gu(1)
        anchors.bottom: parent.bottom
        anchors.bottomMargin: units.gu(1)

        image: Image {
            id: trackArt
            height: parent.height
            width: units.gu(8)
            fillMode: Image.PreserveAspectFit
            anchors.verticalCenter: parent.verticalCenter
        }
    }

    Text {
        id: trackTitle
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: artworkContainer.right
        anchors.right: parent.right
        anchors.leftMargin: units.gu(1)
        anchors.rightMargin: units.gu(1)
        wrapMode: Text.Wrap
        elide: Text.ElideRight
        clip: true
    }

    Text {
        id: trackDuration
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: units.gu(1)
        anchors.bottomMargin: units.gu(1)
        color: UbuntuColors.orange
        font.pixelSize: units.gu(1.2)
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            itemClicked()
        }
    }
}
