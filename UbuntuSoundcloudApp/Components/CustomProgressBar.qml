import QtQuick 2.0

import Ubuntu.Components 1.3

Item {

    property var progress: 0
    property var totalProgress: 0
    property var bufferProgress: 0
    property var totalBuffer: 0

    Rectangle {
        id: bar
        height: parent.height
        width: parent.width
        color: UbuntuColors.lightGrey

        Rectangle {
            id: filledArea
            color: UbuntuColors.orange
            height: parent.height
            width: (parent.width / totalProgress) * progress
            z: 2
        }

        Rectangle {
            id: bufferedArea
            color: UbuntuColors.warmGrey
            height: parent.height
            width: (parent.width / totalBuffer) * bufferProgress
            z: 1
        }
    }
}
