#ifndef CUSTOM_BUFFER
#define CUSTOM_BUFFER

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>


class CustomBufferer : public QObject
{
    Q_OBJECT

public:
    explicit CustomBufferer(QObject *parent = 0);
    Q_INVOKABLE void downloadTrack(QString url, QString outputFileName);
    Q_INVOKABLE void resumeDownload(QString url, QString outputFileName, qint64 totalSize);
    Q_INVOKABLE void cancelDownload(QNetworkReply *response);
    Q_INVOKABLE QString doesCacheFileExist(QString fileName);
    Q_INVOKABLE QString getCachedFilePath(QString fileName);
    Q_INVOKABLE qint64 getFileSizeOnStorage(QString name);

    ~CustomBufferer();

private:
    Q_INVOKABLE float calculateDownloadPercentage(qint64 downloaded, qint64 total);
    Q_INVOKABLE void writeBytes(QByteArray data, QString path);
    Q_INVOKABLE void clearCache(QString fileName);
    Q_INVOKABLE void download(QNetworkReply *response);
    Q_INVOKABLE QString getCacheDirectory();

    QNetworkAccessManager *networkManager;
    QNetworkReply *networkResponse;
    QByteArray currentData;
    QString outputFileName;
    qint64 totalSize;
    qint64 resumeTotalSize;
    qint64 sizeOnResume;
    bool newUrlBroadcasted;
    bool isDownloading;
    bool isResumeRequest;

public Q_SLOTS:
    void networkFinished();
    void networkUpdateProgress(qint64, qint64);
    void networkReplyError(QNetworkReply::NetworkError);
    void networkUpdateRead();

Q_SIGNALS:
    void readyRead();
    void finished(QNetworkReply *reply);
    void error(QNetworkReply::NetworkError error);
    void downloadProgress(qint64 downloaded, qint64 total);
    void broadcastNewUrl(QString url);
    void fileSizeDetermined(qint64 size, QString name);
};


#endif // CUSTOM_BUFFER

