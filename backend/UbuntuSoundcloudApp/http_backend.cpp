#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QUrl>

#include "http_backend.h"


HttpUrlConnection::HttpUrlConnection(QObject *parent) : QObject(parent)
{

}

void HttpUrlConnection::resolveUser(QString username)
{
    QString base = "http://api.soundcloud.com/resolve.json?url=https://soundcloud.com/";
    QString url = base + username + "&client_id=b15be07e0afa1dea748c1cdd63f9e56e";
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(resolveUserReplyFinished(QNetworkReply*)));
    manager->get(QNetworkRequest(QUrl(url)));
}

void HttpUrlConnection::requestUserInfo(QString request_url)
{
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(userInfoRequestReplyFinished(QNetworkReply*)));
    manager->get(QNetworkRequest(QUrl(request_url)));
}

void HttpUrlConnection::requestTracks(QString favorites_url)
{
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(favoritesRequestReplyFinished(QNetworkReply*)));
    manager->get(QNetworkRequest(QUrl(favorites_url)));
}

void HttpUrlConnection::getStreamUrl(QString url)
{
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(streamUrlRequestReplyFinished(QNetworkReply*)));

    manager->get(QNetworkRequest(QUrl(url)));
}

void HttpUrlConnection::resolveUserReplyFinished(QNetworkReply *networkReply)
{
    if (!networkReply->error()) {
        QString response = QString::fromUtf8(networkReply->readAll());
        emit userResolved(response);
    } else {
        QString response = QString::fromUtf8(networkReply->readAll());
        qDebug() << response;
        switch (networkReply->error()) {
        case QNetworkReply::ContentNotFoundError:
            emit userResolveError("404");
            break;
        default:
            emit userResolveError("unknown");
            break;
        }
    }
}

void HttpUrlConnection::userInfoRequestReplyFinished(QNetworkReply *networkReply)
{
    if (!networkReply->error()) {
        QString response = QString::fromUtf8(networkReply->readAll());
        emit userInfoLoaded(response);
    } else {
        QString response = QString::fromUtf8(networkReply->readAll());
        emit userInfoError(response);
    }
}

void HttpUrlConnection::favoritesRequestReplyFinished(QNetworkReply *networkReply)
{
    if (!networkReply->error()) {
        QString response = QString::fromUtf8(networkReply->readAll());
        QString urlPath = networkReply->url().path();
        QStringList array = urlPath.split("?");
        QString a = array[0];
        QStringList array2 = a.split("/");

        emit favoritesLoaded(response, array2[array2.length() - 1]);
    }
}

void HttpUrlConnection::streamUrlRequestReplyFinished(QNetworkReply *networkReply)
{
    if (!networkReply->error()) {
        QString response = QString::fromUtf8(networkReply->readAll());
        QUrl requestUrl = networkReply->url();
        emit streamUrlResolved(response, getTrackIdFromUrl(requestUrl.url()));
    }
}

QString HttpUrlConnection::getTrackIdFromUrl(QString trackUrl)
{
    QStringList array = trackUrl.split("/");
    return array[array.length() - 2];
}

HttpUrlConnection::~HttpUrlConnection() {

}
