#include <QtQml>
#include <QtQml/QQmlContext>

#include "backend.h"
#include "http_backend.h"
#include "custom_buffer.h"


void BackendPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(uri == QLatin1String("UbuntuSoundcloudApp"));

    qmlRegisterType<HttpUrlConnection>(uri, 1, 0, "HttpUrlConnection");
    qmlRegisterType<CustomBufferer>(uri, 1, 0, "CustomBufferer");
}

void BackendPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    QQmlExtensionPlugin::initializeEngine(engine, uri);
}
