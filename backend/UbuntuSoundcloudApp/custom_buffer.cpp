#include <QFile>
#include <QDir>

#include "custom_buffer.h"


CustomBufferer::CustomBufferer(QObject *parent)
    : QObject(parent)
    , networkManager(networkManager)
    , networkResponse(networkResponse)
    , newUrlBroadcasted(false)
    , totalSize(0)
    , outputFileName("unknown")
    , isDownloading(false)
    , resumeTotalSize(0)
    , isResumeRequest(false)
{

}

void CustomBufferer::download(QNetworkReply *response)
{
    connect(response, SIGNAL(readyRead()),
            this, SLOT(networkUpdateRead()));
    connect(response, SIGNAL(error(QNetworkReply::NetworkError)),
            this, SLOT(networkReplyError(QNetworkReply::NetworkError)));
    connect(response, SIGNAL(downloadProgress(qint64, qint64)),
            this, SLOT(networkUpdateProgress(qint64, qint64)));
    connect(response, SIGNAL(finished()),
            this, SLOT(networkFinished()));
}

void CustomBufferer::downloadTrack(QString url, QString outputFileName)
{
    isResumeRequest = false;
    cancelDownload(networkResponse);
    newUrlBroadcasted = false;
    this->outputFileName = outputFileName;
    networkManager = new QNetworkAccessManager;
    networkResponse = networkManager->get(QNetworkRequest(QUrl(url)));
    networkResponse->setReadBufferSize(500);
    isDownloading = true;
    download(networkResponse);
}

void CustomBufferer::resumeDownload(QString url, QString outputFileName, qint64 totalSize)
{
    if (doesCacheFileExist(outputFileName) == "true") {
        resumeTotalSize = totalSize;
        isResumeRequest = true;
        this->outputFileName = outputFileName;
        sizeOnResume = getFileSizeOnStorage(outputFileName);
        QByteArray rangeHeaderValue = "bytes=" + QByteArray::number(sizeOnResume) + "-";
        networkManager = new QNetworkAccessManager;
        QNetworkRequest request = QNetworkRequest(QUrl(url));
        request.setRawHeader("Range", rangeHeaderValue);
        networkResponse = networkManager->get(request);
        networkResponse->setReadBufferSize(500);
        isDownloading = true;
        newUrlBroadcasted = true;
        download(networkResponse);
    }
}

void CustomBufferer::cancelDownload(QNetworkReply *response)
{
    if (isDownloading) {
        disconnect(response, SIGNAL(readyRead()),
                   this, SLOT(networkUpdateRead()));
        disconnect(response, SIGNAL(error(QNetworkReply::NetworkError)),
                   this, SLOT(networkReplyError(QNetworkReply::NetworkError)));
        disconnect(response, SIGNAL(downloadProgress(qint64, qint64)),
                   this, SLOT(networkUpdateProgress(qint64, qint64)));
        disconnect(response, SIGNAL(finished()),
                   this, SLOT(networkFinished()));
        networkResponse->abort();
        isDownloading = false;
    }
}

void CustomBufferer::networkUpdateProgress(qint64 read, qint64 total)
{
    if (isResumeRequest) {
        qDebug() << sizeOnResume + read;
        qDebug() << resumeTotalSize;
        emit downloadProgress(sizeOnResume + read, resumeTotalSize);
    } else {
        qDebug() << read;
        qDebug() << total;
        totalSize = total;
        emit downloadProgress(read, total);
    }
}

void CustomBufferer::networkFinished()
{
    emit finished(networkResponse);
    isDownloading = false;
}

void CustomBufferer::networkReplyError(QNetworkReply::NetworkError err)
{
    emit error(err);
    isDownloading = false;
}

void CustomBufferer::networkUpdateRead()
{
    QString output(getCacheDirectory() + outputFileName + ".mp3");
    QFile file(output);
    qint64 currentFileSize = file.size();
    currentData.clear();
    currentData.append(networkResponse->readAll());
    if (isResumeRequest) {
        if (currentFileSize <= resumeTotalSize) {
            writeBytes(currentData, output);
            emit readyRead();
        }

    } else if (currentFileSize <= totalSize) {
        writeBytes(currentData, output);
        if (!newUrlBroadcasted && calculateDownloadPercentage(currentFileSize, totalSize) > 5) {
            emit fileSizeDetermined(totalSize, outputFileName);
            emit broadcastNewUrl(output);
            newUrlBroadcasted = true;
        }
        emit readyRead();
    }
}

float CustomBufferer::calculateDownloadPercentage(qint64 downloaded, qint64 total)
{
    float divide = (float) downloaded / (float) total;
    return divide * 100;
}

void CustomBufferer::writeBytes(QByteArray data, QString path)
{
    QFile output_file(path);
    output_file.open(QIODevice::ReadWrite);
    output_file.seek(output_file.size());
    output_file.write(data);
    output_file.flush();
    output_file.close();
}

QString CustomBufferer::getCacheDirectory()
{
    QString result("");
    QString localDirectoryPath(QDir::homePath() + "/.local/share/ubuntusoundcloudapp.stefwe/");
    QDir localDirectory(localDirectoryPath);
    if (localDirectory.exists()) {
        localDirectory.cd(localDirectoryPath);
        localDirectory.mkdir("cached");
        return localDirectoryPath + "cached/";
    }
    return result;
}

void CustomBufferer::clearCache(QString fileName)
{
    QFile fileToDelete(getCachedFilePath(fileName));
    if (fileToDelete.exists()) {
        fileToDelete.remove();
    }
}

QString CustomBufferer::doesCacheFileExist(QString fileName)
{
    QFile file(getCachedFilePath(fileName));
    if (file.exists()) {
        return "true";
    } else {
        return "false";
    }
}

QString CustomBufferer::getCachedFilePath(QString fileName)
{
    QString pathRoot(QDir::homePath() + "/.local/share/ubuntusoundcloudapp.stefwe/cached/");
    QString filePath(pathRoot + fileName + ".mp3");
    return filePath;
}

qint64 CustomBufferer::getFileSizeOnStorage(QString name)
{
    QFile path(getCachedFilePath(name));
    if (path.exists()) {
        return path.size();
    } else {
        return 0;
    }
}

CustomBufferer::~CustomBufferer()
{

}
