#ifndef MYTYPE_H
#define MYTYPE_H

#include <QObject>
#include <QtNetwork/QNetworkReply>


class HttpUrlConnection : public QObject
{
    Q_OBJECT

public:
    explicit HttpUrlConnection(QObject *parent = 0);
    Q_INVOKABLE void getStreamUrl(QString url);
    Q_INVOKABLE void resolveUser(QString username);
    Q_INVOKABLE void requestUserInfo(QString username);
    Q_INVOKABLE void requestTracks(QString favorites_url);
    Q_INVOKABLE QString getTrackIdFromUrl(QString trackUrl);

    ~HttpUrlConnection();

private:
    QNetworkAccessManager *manager;

public slots:
    void streamUrlRequestReplyFinished(QNetworkReply*);
    void resolveUserReplyFinished(QNetworkReply*);
    void favoritesRequestReplyFinished(QNetworkReply*);
    void userInfoRequestReplyFinished(QNetworkReply*);

Q_SIGNALS:
    void finished(QNetworkReply* reply);
    void userResolved(QString data);
    void userResolveError(QString data);
    void streamUrlResolved(QString data, QString trackId);
    void favoritesLoaded(QString data, QString type);
    void userInfoLoaded(QString data);
    void userInfoError(QString data);
};

#endif // MYTYPE_H
